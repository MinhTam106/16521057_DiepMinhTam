
#include "Bezier.h"
#include <iostream>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	int x, y;
	for (double i = 0.0; i <= 1; i += 0.002) {
		x = int(p1.x*(1 - i)*(1 - i) + 2 * (1 - i)*i*p2.x + i * i*p3.x + 0.5);
		y = int(p1.y*(1 - i)*(1 - i) + 2 * (1 - i)*i*p2.y + i * i*p3.y +0.5);
		SDL_RenderDrawPoint(ren, x, y);
		
	}
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	int x, y;
	for(double i = 0.0; i <= 1; i += 0.002) {
		
		x = int((1 - i)*(1 - i)*(1 - i)*p1.x + 3 * (1 - i)*(1 - i)*i*p2.x + 3 * (1 - i)*i*i*p3.x + i * i*i*p4.x + 0.5);
		y= int((1 - i)*(1 - i)*(1 - i)*p1.y + 3 * (1 - i)*(1 - i)*i*p2.y + 3 * (1 - i)*i*i*p3.y + i * i*i*p4.y + 0.5);
		SDL_RenderDrawPoint(ren, x, y);
		//SDL_RenderPresent(ren);
	}
}
