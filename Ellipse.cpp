
#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc+x, yc+y);
	SDL_RenderDrawPoint(ren, xc+x, yc-y);
	SDL_RenderDrawPoint(ren, xc-x, yc+y);
	SDL_RenderDrawPoint(ren, xc-x, yc-y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    // Area 1
	Draw4Points(xc, yc,0,b,ren);
	int x = 0, y=b;
	// :) :) :) :)
	int a2=a*a, b2=b*b;
	// :) :) :) :)
	int p=a2+2*b2-2*a2*b;
	// :) :) :)
	while(x*x*(a2+b2)<=a2*a2){
		if(p<0)
			p+=4*b2*x+6*b2;
		else{
			p+=4*b2*x+6*b2+4*a2-4*a2*y;
			y-=1;
		}
		x+=1;
		Draw4Points(xc, yc, x,y,ren);
	}
	 // Area 2
	Draw4Points(xc,yc,a,0,ren);
	p = 2*a2 + b2-2*a*b2;
	x=a; y = 0;
	while(x*x*(a2+b2)>a2*a2){
		if(p <=0) 
			p+=4*a2*y+6*a2;
		else{
			p+=4*b2-4*b2*x+4*a2*y+6*a2;
			x-=1;
		}
		y+=1;
		Draw4Points(xc,yc,x,y,ren);
	}
   
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    // Area 1
	Draw4Points(xc,yc,0, b,ren);
	int x = 0, y =b;
	int a2=a*a, b2=b*b;
	float p = b2-a2*b+float(a2/4);
	while(x*x*(a2+b2) <= a2*a2){
		if(p<=0)
			p+=2*b2*x+3*b2;
		else{
			p+=2*b2*x -2*a2*y + 3*b2 +2*a2;
			y-=1;
		}
		x+=1;
		Draw4Points(xc,yc,x,y,ren);
	}
    // Area 2
	Draw4Points(xc, yc, a, 0,ren);
	x=a; y = 0; // :) :) :) :) :) :) :) :) :) :)
	p=a2 - b2*a + float(b2/4);
	while(x*x*(a2+b2)> a2*a2){
		if(p <= 0 )
			p+=a2*(2*y+3);
		else{
			p+=-b2*(2*x-2)+a2*(2*y+3);
			x-=1;
		}
		y+=1;
		Draw4Points(xc, yc, x, y,ren);
	}


}
